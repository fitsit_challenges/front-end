# Front-end engineering challenge

## Delivarables
Design and Implement a game of [sudoku](https://en.wikipedia.org/wiki/Sudoku) using React and use Redux for state management.

### Features

- There should exist a function to initialize a new game with a randomly generated state
- The game should be playable with a Call to Action for validating the current state of the game.

### Bonus 

- Adjustable difficulty level
- User should be able to create their own game (Use one of the client-side available storages)


## Delivery

Your application can be sent to us as a GitHub repository or as a compressed archive containing all the deliverables.

## Review session

After receiving your code challenge, we organize a review session with you and a few engineers from FitSit. During the review session, we will:

- Ask you to share you screen and do a quick demo of the app you built
- Ask you to present your project structure and walk us through the code (the different components, the state management, etc.)
- Ask you general technical questions related to your project and frontend architecture

A few examples of the topic that we like to discuss in more details:

- Scaling of an SPA
- Websockets
- State management
- UI libraries
- Styling
- Testing